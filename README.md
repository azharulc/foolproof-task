Foolproof Task
==============

Steps to run the project
------------------------
1. Clone project locally
2. `cd` into project directory
3. Run `npm install` to get dependencies
4. Run `npm start` to kick off the dev build and launch a dev server locally
