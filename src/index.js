import Header from "./components/header/header";
import Form from "./components/form/form";

require('./index.scss');

const container = document.getElementById("container");

const header = new Header("Spread the word!", "Our recipes are awesome, sign up to out newsletter and invite friends to join the Gilled Prawn cult");
const form = new Form("name");
//const content = document.getElementById('main-content');
// content.textContent = 'inset text here.';

container.appendChild(header.render());
container.appendChild(form.render());
