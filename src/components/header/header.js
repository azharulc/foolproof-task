import DomElement from "../dom-element/dom-element";

require('./header.scss');

export default class Header extends DomElement {
  constructor(headingCopy, bodyCopy) {
    super();

    this.heading = headingCopy;
    this.bodyCopy = bodyCopy;
  }

  render() {
    const html = `<header class="page-header">
      <h1 class="page-header__heading">${this.heading}</h1>
      <p class="page-header__body">${this.bodyCopy}</p>
    </header>`;
    const element = this.htmlToElement(html);

    return element;
  }
}
