// Allows all form elements to be easily created & manipulated
export default class DomElement {
  htmlToElement(html) {
    const template = document.createElement('div');
    template.innerHTML = html;

    return template.firstChild;
  }
}
