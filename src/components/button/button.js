import DomElement from "../dom-element/dom-element";

require('./button.scss');

export default class Button extends DomElement {
  constructor(buttonCopy, buttonType) {
    super();

    this.buttonCopy = buttonCopy;
    this.buttonType = buttonType;
    this.button = null;

    this.createButton();
  }

  createButton() {
    // Creates a button depending on configuration
    if (this.buttonType.buttonType == "add") {
      this.button = `<button class="button">${this.buttonCopy}</button>`;
    } else if (this.buttonType.buttonType == "remove") {
      this.button = `<button class="button button--remove"><span class="fas fa-trash"></span>&nbsp;&nbsp;&nbsp;&nbsp;${this.buttonCopy}</button>`;
    } else if (this.buttonType.buttonType == "submit") {
      this.button = `<button class="button button--submit" type="submit">${this.buttonCopy}&nbsp;&nbsp;&nbsp;&nbsp;<span class="fas fa-arrow-right"></span></button>`;
    }
  }

  render() {
    const element = this.htmlToElement(this.button);

    return element;
  }
}
