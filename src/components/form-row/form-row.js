import DomElement from "../dom-element/dom-element";
import TextInput from "../text-input/text-input";
import Button from "../button/button";

require('./form-row.scss');

export default class FormRow extends DomElement {
  // must pass in an an array of label & name objects as config for rows
  constructor(nameKeys, maxRows) {
    super();

    this.nameKeys = nameKeys; // used for config
    this.formRows = []; // used to keep track of rows
    this.maxRows = maxRows; // limits the number of rows
    this.uniqueCount = 0; // this is to ensure that ids will always be unique

    const firstRow = this.createRow(this.formRows.length);
    this.formRows.push(firstRow);
  }

  onAddRowClick() {
    event.preventDefault();
    this.formRows.push(this.createRow(this.formRows.length));

    const buttonContainer = document.getElementById('button-container');
    const parent = document.getElementById('news-letter-form');

    parent.insertBefore(this.formRows[this.formRows.length - 1], buttonContainer);
    this.updateRowButton();
  }

  onRemoveClick() {
    event.preventDefault();

    const parent = document.getElementById('news-letter-form');
    const currentRow = event.srcElement.parentNode.parentNode;
    const index = this.formRows.indexOf(currentRow);
    // Remove row from DOM and tracker array
    this.formRows.splice(currentRow, 1);
    parent.removeChild(currentRow);

    if (this.formRows.length === this.maxRows - 1) {
      this.toggleAddButton(this.formRows.length - 1, false);
    }
  }

  updateRowButton() {
    const totalRows = this.formRows.length;
    const newRowIndex = totalRows - 1;
    const previousRowIndex = newRowIndex - 1;

    const removeButton = this.createRowButton("Remove", "remove");

    this.replaceButton(previousRowIndex, removeButton);
    // Remove ability to add another row at max row size
    if (totalRows === this.maxRows) {
      this.toggleAddButton(newRowIndex, true);
    }
  }

  toggleAddButton(rowIndex, isDisabled) {
    const rowButton = this.formRows[rowIndex].elements.item(3);

    if (isDisabled) {
      rowButton.classList.add("button--disabled");
    } else {
      rowButton.classList.remove("button--disabled");
    }

    rowButton.disabled = isDisabled;
  }

  getRowButton(rowIndex) {

  }

  replaceButton(rowIndex, newButton) {
    // child node index 3 targets the button at the end of the row
    const buttonContainer = this.formRows[rowIndex].childNodes[3];
    const buttonToReplace = this.formRows[rowIndex].elements.item(3);

    buttonContainer.replaceChild(newButton, buttonToReplace);
  }

  createRowButton(text, type) {
    const button = new Button(text, {
      buttonType: type
    });
    const buttonElement = button.render();

    buttonElement.addEventListener('click', () => {
      if (type == "add") {
        this.onAddRowClick();
      } else if (type == "remove") {
        this.onRemoveClick();
      }
    });

    return buttonElement;
  }

  createRow(rowCount) {
    this.uniqueCount += 1;
    // Create fieldset
    const fieldsetHtml = `<fieldset class="form-row"></fieldset>`;
    const fieldsetElement = this.htmlToElement(fieldsetHtml);
    const rowButtonContainer = `<div class="form-row--button-container"></div>`;
    const rowButtonContainerElement = this.htmlToElement(rowButtonContainer);

    // Add input items to row
    this.nameKeys.forEach((value, index) => {
      let input = new TextInput(value.label, value.name, this.uniqueCount);

      fieldsetElement.appendChild(input.render());
    });

    // Create row button & add to row
    const buttonElement = this.createRowButton("Add Friend", "add");
    rowButtonContainerElement.appendChild(buttonElement);

    fieldsetElement.appendChild(rowButtonContainerElement);

    return fieldsetElement;
  }

  render() {
    return this.formRows[0];
  }
}
