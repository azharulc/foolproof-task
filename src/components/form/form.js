import DomElement from "../dom-element/dom-element";
import FormRow from "../form-row/form-row";
import Button from "../button/button";

require('./form.scss');

export default class Form extends DomElement {
  constructor(name) {
    super();

    this.name = name;
  }

  render() {
    // Create the form elements
    const formHtml = `<form id="news-letter-form" class="form" action="#" method="post"></form>`;
    const formElement = this.htmlToElement(formHtml);
    const submitContainer = `<div class="form--button-container" id="button-container"></div>`;
    const submitContainerElement = this.htmlToElement(submitContainer);
    // Define the rows
    const nameKeys = [
      {
        label: "Name",
        name: "name"
      },
      {
        label: "Surname",
        name: "surname"
      },
      {
        label: "Email Address",
        name: "email"
      }
    ];
    const formRow = new FormRow(nameKeys, 5);
    const sendButton = new Button("Send", {buttonType: "submit"});
    const sendButtonElement = sendButton.render();

    sendButtonElement.addEventListener('click', () => {
      this.onSubmit();
    });
    formElement.appendChild(formRow.render());
    submitContainerElement.appendChild(sendButtonElement);
    formElement.appendChild(submitContainerElement);

    return formElement;
  }

  onSubmit() {
    // Get the data ready to submit
    event.preventDefault();

    const form = document.getElementById("news-letter-form").elements;

    console.log(form); //log left in as example of data ready for posting
  }
}
