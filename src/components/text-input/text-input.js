import DomElement from "../dom-element/dom-element";

require('./text-input.scss');

export default class TextInput extends DomElement {
  constructor(label, name, uniqueIdentifier) {
    super();

    this.label = label;
    this.name = name;
    this.uniqueIdentifier = uniqueIdentifier; // used to make ids unique
  }

  render() {
    const html = `<div class="label-input">
      <label for="${this.name}-${this.uniqueIdentifier}" class="label-input__label">${this.label}</label>
      <input type="text" name="${this.name}[]" id="${this.name}-${this.uniqueIdentifier}" class="label-input__input" />
    </div>`;

    const element = this.htmlToElement(html);

    return element;
  }
}
